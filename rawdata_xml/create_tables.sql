
CREATE SCHEMA IF NOT EXISTS rawdata_xml;

create sequence if not exists rawdata_xml.seq_overview_filid AS bigint;


/*---------------------------------------------------------tables used for multiple file types---------------------------------------------------------*/


--drop table rawdata_xml.master cascade;
create table rawdata_xml.master
(
	filid bigint,
	machine_key text,
	created timestamptz default now()
);


--drop table rawdata_xml.overview;
create table rawdata_xml.overview
(
	filid bigint NOT NULL DEFAULT nextval('rawdata_xml.seq_overview_filid'::regclass) ,
	doc_mod_date timestamptz,
	filename text,
	source_id text,
	stanford_version text,
	unique(machine_key, filename, doc_mod_date),
	CONSTRAINT overview_pkey PRIMARY KEY (filid)
)
inherits (rawdata_xml.master);





--drop table rawdata_xml.machine_info;
create table rawdata_xml.machine_info
(
	doc_mod_date timestamptz,
	machine_category text,
	machine_user_id text,
	machine_application_version text,
	machine_base_manufacturer text,
	machine_base_model text,
	base_machine_manufacturer_id text,
	machine_head_manufacturer text,
	machine_head_model text,
	unique(machine_key)
)
inherits (rawdata_xml.master);


--drop table rawdata_xml.objects;
create table rawdata_xml.objects
(
	doc_mod_date timestamptz,
	start_date timestamptz,
	object_mod_date timestamptz,
	object_key int,
	object_user_id text,
	object_name text,
	logging_form_code text,
	logging_org_business_id text,
	forest_owner_first_name text,
	forest_owner_last_name text,
	forest_owner_email text,
	forest_owner_bussiness_id text,
	contract_number text,
	unique(machine_key, object_key),
	FOREIGN KEY (machine_key) REFERENCES rawdata_xml.machine_info (machine_key)
)
inherits (rawdata_xml.master);




--drop table rawdata_xml.sub_objects;
create table rawdata_xml.sub_objects
(
	doc_mod_date timestamptz,
	object_key int,
	sub_object_key int,
	sub_object_user_id text,
	sub_object_name text,
	logging_form_code text,
	unique(machine_key, object_key, sub_object_key),
	FOREIGN KEY (machine_key, object_key) REFERENCES rawdata_xml.objects (machine_key, object_key)
)
inherits (rawdata_xml.master);


--drop table rawdata_xml.species;
create table rawdata_xml.species
(
	doc_mod_date timestamptz,
	species_group_mod_date timestamptz,
	species_group_key int,
	species_group_user_id text,
	species_group_name text,
	species_group_info text,
	species_group_version text,
	unique(machine_key, species_group_key),
	FOREIGN KEY (machine_key) REFERENCES rawdata_xml.machine_info (machine_key)
)
inherits (rawdata_xml.master);


--drop table rawdata_xml.products;
create table rawdata_xml.products
(
	doc_mod_date timestamptz,
	product_key int,
	product_creation_date timestamptz,
	modification_date timestamptz,
	product_version text,
	product_name text,
	product_user_id text,
	product_group_name text,
	product_info text,
	buyer_business_id text,
	logging_org_business_id text,
	product_destination_business_id text,
	species_group_key int,
	unique(machine_key, product_key ),
	FOREIGN KEY (machine_key, species_group_key) REFERENCES rawdata_xml.species (machine_key, species_group_key)
)
inherits (rawdata_xml.master);

--drop table rawdata_xml.operators;
create table rawdata_xml.operators
(
	doc_mod_date timestamptz,
	operator_key int,
	operator_user_id text,
	operator_first_name text,
	operator_last_name text,
	unique(machine_key, operator_key),
	FOREIGN KEY (machine_key) REFERENCES rawdata_xml.machine_info (machine_key)
)
inherits (rawdata_xml.master);


/*---------------------------------------------------------tables used for hpr---------------------------------------------------------*/

--drop table rawdata_xml.hpr_stems;
create table rawdata_xml.hpr_stems
(
	doc_mod_date timestamptz,
	stem_key int,
	object_key int,
	sub_object_key int,
	species_group_key int,
	operator_key int,
	harvest_date timestamptz,
	dbh int,
	machine_position geography(point, 4326),
	crane_tip_position geography(point, 4326),
	unique(machine_key, stem_key),
	FOREIGN KEY (machine_key, species_group_key) REFERENCES rawdata_xml.species (machine_key, species_group_key),
	FOREIGN KEY (machine_key, object_key, sub_object_key) REFERENCES rawdata_xml.sub_objects (machine_key, object_key, sub_object_key)
)
inherits (rawdata_xml.master);



--drop table rawdata_xml.hpr_logs;
create table rawdata_xml.hpr_logs
(
	doc_mod_date timestamptz,
	stem_key int,
	log_key int,
	product_key int,
	m3_price double precision,
	m3_sob double precision,
	m3_sub double precision,
	length int,
	butt_ob int,
	butt_ub int,
	mid_ob int,
	mid_ub int,
	top_ob int,
	top_ub int,
	unique(machine_key, stem_key, log_key),
	FOREIGN KEY (machine_key, stem_key) REFERENCES rawdata_xml.hpr_stems (machine_key, stem_key),
	FOREIGN KEY (machine_key, product_key) REFERENCES rawdata_xml.products (machine_key, product_key)
)
inherits (rawdata_xml.master);


/*---------------------------------------------------------tables used for fpr---------------------------------------------------------*/



--drop table rawdata_xml.fpr_locations;
create table rawdata_xml.fpr_locations
(
	doc_mod_date timestamptz,
	location_key int,
	object_key int,
	sub_object_key int,
	location_user_id text,
	location_name text,
	position geography(point, 4326),
	unique(machine_key, location_key),
	FOREIGN KEY (machine_key, object_key, sub_object_key) REFERENCES rawdata_xml.sub_objects (machine_key, object_key, sub_object_key)
)
inherits (rawdata_xml.master);


--drop table rawdata_xml.fpr_deliveries;
create table rawdata_xml.fpr_deliveries
(
	doc_mod_date timestamptz,
	delivery_key int,
	delivery_user_id text,
	delivery_name text,
	modification_date timestamptz,
	product_key int,
	unique(machine_key, delivery_key),
	FOREIGN KEY (machine_key, product_key) REFERENCES rawdata_xml.products (machine_key, product_key)
)
inherits (rawdata_xml.master);

--drop table rawdata_xml.fpr_loads;
create table rawdata_xml.fpr_loads
(
	doc_mod_date timestamptz,
	load_key int,
	operator_key int,
	load_number int,
	unloading_time timestamptz,
	unique(machine_key, load_key),
	FOREIGN KEY (machine_key, operator_key) REFERENCES rawdata_xml.operators (machine_key, operator_key)
)
inherits (rawdata_xml.master);



--drop table rawdata_xml.fpr_partial_loads;
create table rawdata_xml.fpr_partial_loads
(
	doc_mod_date timestamptz,
	load_key int,
	partial_load_key int,
	delivery_key int,
	location_key int,
	m3_sub double precision,
	m3_sob double precision,
	unique(machine_key, load_key,partial_load_key),
	FOREIGN KEY (machine_key, load_key) REFERENCES rawdata_xml.fpr_loads (machine_key, load_key),
	FOREIGN KEY (machine_key, delivery_key) REFERENCES rawdata_xml.fpr_deliveries (machine_key, delivery_key),
	FOREIGN KEY (machine_key, location_key) REFERENCES rawdata_xml.fpr_locations (machine_key, location_key)
)
inherits (rawdata_xml.master);





