


create or replace function xml.create_point(lat double precision, lon double precision, lat_cat text, lon_cat text) returns geography(point, 4326) as
$body$
DECLARE
BEGIN

IF lat_cat = 'South' THEN
	lat = lat * -1;
END IF;
IF lon_cat = 'WEST' THEN
	lon = lon * -1;
END IF;

return ST_SetSrid(ST_Point(lon, lat), 4326);

END
$body$
LANGUAGE plpgsql;
