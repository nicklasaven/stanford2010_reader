﻿




create or replace function xml.spider
(
	data_in text, 
	format_in text, 
	path_in text, 
	source_id_in text
) returns int as
$body$
DECLARE
	xml_ver float;
	sf_standard stanford_standard;
	file_ending text;
	uncompr_data text;
	sf2010_fileendings text[]= '{"hpr", "fpr","hqc","fqc","mom","oin", "pin", "spi", "ogi", "foi", "fdi", "udi", "thp", "ogr"}'::text[];
	classic_fileendings text[]  = '{"prd", "pri","prl","drf","ktr","apt", "stm", "tid", "rep", "mas", "avs", "sti", "kau", "kal", "psu", "inv", "oai", "ghd", "spp", "ap1"}'::text[];
BEGIN

file_ending = right(lower(path_in),3);
IF file_ending = any (classic_fileendings) THEN
	/*send to C-parser*/
--	RAISE NOTICE 'classic';
	return 0;
ELSIF file_ending = any (sf2010_fileendings) THEN

--	RAISE NOTICE '2010';
	uncompr_data = fil_data.show_file(data_in,format_in,'2010');
	xml_ver = (regexp_matches(left(uncompr_data, 1000),'(?<=\<\?xml.*\?>.*version ?= ?")\d.\d','i'))[1];

	IF xml_ver < 3 THEN
	
--	RAISE NOTICE 'v2';
	CASE file_ending
		WHEN 'hpr' THEN
			PERFORM xml.hpr_v2(uncompr_data::xml, format_in, source_id_in);
			return 0;
		WHEN 'fpr' THEN
			PERFORM xml.fpr_v2(uncompr_data::xml, format_in, source_id_in);
			return 0;
		ELSE
			return 0;
		END CASE;

	ELSIF xml_ver::float >= 3 THEN
	
--	RAISE NOTICE 'v3';
	CASE file_ending
		WHEN 'hpr' THEN
			PERFORM xml.hpr_v3(uncompr_data::xml, format_in, source_id_in);
			return 0;
		WHEN 'fpr' THEN
			PERFORM xml.fpr_v3(uncompr_data::xml, format_in, source_id_in);
			return 0;
		ELSE
			return 0;
		END CASE;
	END IF;
ELSE

--	RAISE NOTICE 'No known file ending %', file_ending;
	return 0;
END IF;
return 0;

END
$body$
language plpgsql





--select pg_backend_pid()