﻿/*
CREATE TYPE public.stanford_standard AS ENUM
   ('classic',
    '2010',
    'unknown');
*/



CREATE OR REPLACE FUNCTION fil_data.show_file(
    the_data text,
    format text,
    standard stanford_standard)
  RETURNS text AS
$BODY$
DECLARE 
	fn text;
	comp text;
	enc text;
	enc_str text;
	enc_string text;
	decoded_data bytea;
	the_beginning bytea;
	enc_startpos int;
	enc_len int;
BEGIN

	IF format ilike '%gzip%' THEN
		decoded_data =  gunzip_bytea(decode(the_data,'base64'));
	ELSE
		decoded_data = decode(the_data,'base64');
	END if;
	
	IF lower(standard::text) = 'classic' THEN

		the_beginning = substring(decoded_data for 50);
		enc_startpos = position('~1 3' in the_beginning)  + 1 ;
		
		IF enc_startpos = 1 THEN
			enc='LATIN1'; --If we cannot find the encoding in a classic file we just try using Latin1
		ELSE

			enc_len = position('~' in substring(the_beginning from enc_startpos for 50)) ;

			enc_string = convert_from(substring(the_beginning from enc_startpos for enc_len), 'UTF8');

			enc_str = (regexp_match(enc_string,'(?<=.*)\d*?d*-?\d*?(?=~)','i'))[1]; --We temporary count on that it is UTF8. That is ok since there cannot be any strange characters in the beginning of the file

			IF length(enc_str) > 0 THEN
				IF left(enc_str, 4) = '8859' THEN
					enc='LATIN';
					IF enc_str ~ '-\d+' then 
						enc = enc || (regexp_match(enc_str, '\d+$'))[1];
					ELSE
						enc = 'LATIN1';
					END IF;
				ELSE
					enc = enc_str;
				END IF;
			ELSE
				enc = 'LATIN1';
			END IF;								 
		END IF;
	ELSE
		enc='UTF8';
		decoded_data = btrim(decoded_data, decode('efbbbf', 'hex'));

	END IF;
	RETURN convert_from(decoded_data, enc);

	EXCEPTION
		WHEN others THEN
			RAISE NOTICE 'Failed to gunzip or convert text to proper encoding. No encoding found from %', enc;
		RETURN null;
END
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;
ALTER FUNCTION fil_data.show_file(text, text, stanford_standard)
  OWNER TO nicklas;