

/*As long as there is no difference between v2 and v3 we just send it to v2 to avoid maintaining 2 versions.
If we need to treat them individually, just copy v2 to here and do the changes;*/

CREATE OR REPLACE function xml.fpr_v3(d xml, filename text default null, source_id text default null) RETURNS INT AS
	'SELECT xml.fpr_v2(d , filename , source_id )'
LANGUAGE sql;
