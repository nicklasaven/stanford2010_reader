
CREATE OR REPLACE function xml.hpr_v2(d xml, filename text default null, source_id text default null) RETURNS INT AS
$body$
DECLARE
	row_path TEXT;
	mk TEXT;
	machine_index INT;
	doc_mod_date timestamptz;
	stanford_version text;
	machine_key_val text;
	machine_user_id_val text;
	machine_application_version_val text;
	machine_base_manufacturer_val text;
	machine_base_model_val text;
	base_machine_manufacturer_id_val text;
	machine_head_manufacturer_val text;
	machine_head_model_val text;
	machine_category_val text;
	filid_val bigint = null;
BEGIN

/* doc_mod_date is constant for the whole file so we collect it once*/

SELECT (xpath('/x:HarvestedProduction/x:HarvestedProductionHeader/x:ModificationDate/text()', d, ARRAY[ARRAY['x', 'urn:skogforsk:stanford2010']]))[1] INTO doc_mod_date;
SELECT (xpath('/x:HarvestedProduction/@version', d, ARRAY[ARRAY['x', 'urn:skogforsk:stanford2010']]))[1] INTO stanford_version;

/* LOOP over machine level in file. The StanforD standard permits multiple machines in a file so we have to count on that, even if it shouldn't be possible in reality*/

FOR machine_category_val,mk, machine_index ,machine_user_id_val, machine_application_version_val,machine_base_manufacturer_val,machine_base_model_val,
	base_machine_manufacturer_id_val,machine_head_manufacturer_val,machine_head_model_val IN  
	SELECT machine_category,machine_key, n, machine_user_id, machine_application_version,machine_base_manufacturer,machine_base_model,
	base_machine_manufacturer_id,machine_head_manufacturer,machine_head_model
	FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' AS x),
	'/x:HarvestedProduction/x:Machine[@machineCategory="Harvester"]'
	PASSING  d
	COLUMNS 
	machine_category TEXT path '@machineCategory',
	machine_key TEXT path 'x:MachineKey',
	machine_user_id TEXT path 'x:MachineUserID',	
	machine_application_version text path 'x:MachineApplicationVersion',
	machine_base_manufacturer text path 'x:MachineBaseManufacturer',
	machine_base_model text path 'x:MachineBaseModel',
	base_machine_manufacturer_id text path 'x:BaseMachineManufacturerID',
	machine_head_manufacturer text path 'x:MachineHeadManufacturer',
	machine_head_model text path 'x:MachineHeadModel',
	n FOR ORDINALITY)
LOOP


/*Insert file overview*/
insert into rawdata_xml.overview(doc_mod_date, machine_key,filename,source_id, stanford_version)
values(doc_mod_date, mk, filename,source_id, stanford_version)
ON CONFLICT DO NOTHING
returning filid into filid_val;

if filid_val = 0 then
	return 1;
end if;

/*Insert machine_info*/
insert into rawdata_xml.machine_info(filid,doc_mod_date , machine_category, machine_key ,machine_user_id ,machine_application_version ,machine_base_manufacturer ,machine_base_model,base_machine_manufacturer_id ,machine_head_manufacturer ,machine_head_model)
values(filid_val,doc_mod_date, machine_category_val, mk, machine_user_id_val ,machine_application_version_val ,machine_base_manufacturer_val ,machine_base_model_val ,base_machine_manufacturer_id_val ,machine_head_manufacturer_val ,machine_head_model_val)
ON CONFLICT (machine_key) DO UPDATE SET
filid=EXCLUDED.filid, 
doc_mod_date=EXCLUDED.doc_mod_date,
machine_user_id =EXCLUDED.machine_user_id ,machine_application_version =EXCLUDED.machine_application_version ,
machine_base_manufacturer =EXCLUDED.machine_base_manufacturer,machine_base_model =EXCLUDED.machine_base_model ,base_machine_manufacturer_id =EXCLUDED.base_machine_manufacturer_id ,
machine_head_manufacturer =EXCLUDED.machine_head_manufacturer ,machine_head_model=EXCLUDED.machine_head_model WHERE machine_info.doc_mod_date < EXCLUDED.doc_mod_date;


/*Insert objects*/

row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:ObjectDefinition';

WITH objects as 
(
SELECT * 
FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		start_date timestamptz path 'x:StartDate',
		object_mod_date timestamptz path 'x:ObjectModificationDate',
		object_key int path 'x:ObjectKey',
		object_user_id text path 'x:ObjectUserID',
		object_name text path 'x:ObjectName',		
		logging_form_code text path 'x:LoggingForm/x:LoggingFormCode',		
		logging_org_business_id text path 'x:LoggingOrganisation/x:ContactInformation/x:BusinessID',
		forest_owner_first_name text path 'x:ForestOwner/x:FirstName',
		forest_owner_last_name text path 'x:ForestOwner/x:LastName',
		forest_owner_email text path 'x:ForestOwner/x:Email',
		forest_owner_bussiness_id text path 'x:ForestOwner/x:BusinessID',
		contract_number text path 'x:ContractNumber',
		object_definition xml path '.'		
	) r
)
,i_objects as 
(

	INSERT INTO rawdata_xml.objects(filid,doc_mod_date,machine_key,start_date, object_mod_date, object_key, object_user_id, object_name, logging_form_code,logging_org_business_id , forest_owner_first_name, forest_owner_last_name,forest_owner_email, forest_owner_bussiness_id,contract_number  )
	SELECT filid_val,doc_mod_date,mk,start_date, object_mod_date, object_key, object_user_id, object_name, logging_form_code,logging_org_business_id , forest_owner_first_name, forest_owner_last_name,forest_owner_email, forest_owner_bussiness_id,contract_number  
	FROM objects 
	ON CONFLICT  (machine_key, object_key) DO UPDATE SET 
	filid=EXCLUDED.filid, 
	doc_mod_date=EXCLUDED.doc_mod_date,object_mod_date=EXCLUDED.object_mod_date, object_user_id=EXCLUDED.object_user_id, object_name=EXCLUDED.object_name,
	logging_form_code=EXCLUDED.logging_form_code,logging_org_business_id=EXCLUDED.logging_org_business_id , forest_owner_first_name=EXCLUDED.forest_owner_first_name, 
	forest_owner_last_name=EXCLUDED.forest_owner_last_name,forest_owner_email=EXCLUDED.forest_owner_email, forest_owner_bussiness_id=EXCLUDED.forest_owner_bussiness_id,contract_number=EXCLUDED.contract_number WHERE objects.doc_mod_date<EXCLUDED.doc_mod_date
)
,sub_objects as 
(
SELECT objects.object_key, r.* FROM 
objects, XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	'/x:ObjectDefinition/x:SubObject'
	PASSING  objects.object_definition
	COLUMNS 
		sub_object_key int path 'x:SubObjectKey',
		sub_object_user_id text path 'x:SubObjectUserID',
		sub_object_name text path 'x:SubObjectName',		
		logging_form_code text path 'x:LoggingForm/x:LoggingFormCode'
	) r
)
INSERT INTO rawdata_xml.sub_objects(filid,doc_mod_date, machine_key, object_key, sub_object_key, sub_object_user_id, sub_object_name, logging_form_code)
SELECT filid_val,doc_mod_date, mk, object_key, sub_object_key, sub_object_user_id, sub_object_name, logging_form_code FROM sub_objects
	ON CONFLICT  (machine_key, object_key, sub_object_key) DO UPDATE SET 
	filid=EXCLUDED.filid, 
	doc_mod_date=EXCLUDED.doc_mod_date, sub_object_user_id=EXCLUDED.sub_object_user_id, sub_object_name=EXCLUDED.sub_object_name,logging_form_code=EXCLUDED.logging_form_code WHERE sub_objects.doc_mod_date<EXCLUDED.doc_mod_date;


/*Insert operators*/
row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:OperatorDefinition';

INSERT INTO rawdata_xml.operators(filid,doc_mod_date,machine_key,operator_key, operator_user_id, operator_first_name, operator_last_name)
SELECT filid_val,doc_mod_date,mk, operator_key, operator_user_id, operator_first_name, operator_last_name
FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		operator_key int path 'x:OperatorKey',
		operator_user_id text path 'x:OperatorUserID',
		operator_first_name text path 'x:ContactInformation/x:FirstName',
		operator_last_name text path 'x:ContactInformation/x:LastName'
	) r
	ON CONFLICT  (machine_key, operator_key) DO UPDATE SET 
	filid=EXCLUDED.filid, 
	doc_mod_date=EXCLUDED.doc_mod_date, 
	operator_user_id=EXCLUDED.operator_user_id,
	operator_first_name=EXCLUDED.operator_first_name,
	operator_last_name=EXCLUDED.operator_last_name
	WHERE operators.doc_mod_date<EXCLUDED.doc_mod_date;


/*Insert species*/
row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:SpeciesGroupDefinition';

INSERT INTO rawdata_xml.species(filid,doc_mod_date,machine_key,species_group_mod_date,species_group_key,species_group_user_id,species_group_name, species_group_info, species_group_version)
SELECT filid_val,doc_mod_date,mk, species_group_mod_date,species_group_key,species_group_user_id,species_group_name, species_group_info, species_group_version
FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		species_group_mod_date timestamptz path 'x:SpeciesGroupModificationDate',
		species_group_key int path 'x:SpeciesGroupKey',
		species_group_user_id text path 'x:SpeciesGroupUserID',
		species_group_name text path 'x:SpeciesGroupName',
		species_group_info text path 'x:SpeciesGroupInfo',
		species_group_version text path 'x:SpeciesGroupVersion'
	) r
	ON CONFLICT  (machine_key, species_group_key) DO UPDATE SET 
	filid=EXCLUDED.filid, 
	doc_mod_date=EXCLUDED.doc_mod_date, 
	species_group_mod_date=EXCLUDED.species_group_mod_date, species_group_user_id=EXCLUDED.species_group_user_id,species_group_name=EXCLUDED.species_group_name,
	species_group_info=EXCLUDED.species_group_info, species_group_version=EXCLUDED.species_group_version
	WHERE species.doc_mod_date<EXCLUDED.doc_mod_date;






/*Insert products*/
row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:ProductDefinition';

INSERT INTO rawdata_xml.products(filid,doc_mod_date,machine_key,product_key,product_creation_date,modification_date,product_version,product_name,product_user_id,product_group_name,product_info,buyer_business_id,logging_org_business_id,product_destination_business_id,species_group_key)
SELECT filid_val,doc_mod_date,mk, product_key,product_creation_date,modification_date,product_version,product_name,product_user_id,product_group_name,product_info,buyer_business_id,logging_org_business_id,product_destination_business_id,species_group_key
FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		product_creation_date timestamptz path 'x:ClassifiedProductDefinition/x:ProductCreationDate',
		modification_date timestamptz path 'x:ClassifiedProductDefinition/x:ModificationDate',
		product_key int path 'x:ProductKey',
		product_version text path 'x:ClassifiedProductDefinition/x:ProductVersion',
		product_name text path 'x:ClassifiedProductDefinition/x:ProductName',
		product_user_id text path 'x:ClassifiedProductDefinition/x:ProductUserID',
		product_group_name text path 'x:ClassifiedProductDefinition/x:ProductGroupName',		
		product_info text path 'x:ClassifiedProductDefinition/x:ProductInfo',
		buyer_business_id text path 'x:ClassifiedProductDefinition/x:ProductBuyer/x:BusinessID',
		logging_org_business_id text path 'x:ClassifiedProductDefinition/x:LoggingOrganisation/x:BusinessID',
		product_destination_business_id text path 'x:ClassifiedProductDefinition/x:ProductDestination/x:BusinessID',
		species_group_key int path 'x:ClassifiedProductDefinition/x:SpeciesGroupKey'
	) r
	ON CONFLICT  (machine_key, product_key) DO UPDATE SET 
	filid=EXCLUDED.filid, 
	doc_mod_date=EXCLUDED.doc_mod_date, 
	product_creation_date=EXCLUDED.product_creation_date, modification_date=EXCLUDED.modification_date,product_version=EXCLUDED.product_version,
	product_name=EXCLUDED.product_name, product_user_id=EXCLUDED.product_user_id,
	product_group_name=EXCLUDED.product_group_name, product_info=EXCLUDED.product_info,
	buyer_business_id=EXCLUDED.buyer_business_id, logging_org_business_id=EXCLUDED.logging_org_business_id,
	product_destination_business_id=EXCLUDED.product_destination_business_id, species_group_key=EXCLUDED.species_group_key
	WHERE products.doc_mod_date<EXCLUDED.doc_mod_date;


/*Insert Stems*/
row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:Stem';

INSERT INTO rawdata_xml.hpr_stems(filid,doc_mod_date,machine_key,stem_key,object_key,sub_object_key,species_group_key,operator_key,harvest_date, dbh, machine_position, crane_tip_position)
SELECT filid_val,doc_mod_date,mk, stem_key,object_key,sub_object_key,species_group_key,operator_key,harvest_date, dbh, xml.create_point(lat_base, lon_base, lat_cat_base, lon_cat_base), xml.create_point(lat_crane, lon_crane, lat_cat_crane, lon_cat_crane)
FROM 
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		stem_key int path 'x:StemKey',
		object_key int path 'x:ObjectKey',
		sub_object_key int path 'x:SubObjectKey',
		species_group_key int path 'x:SpeciesGroupKey',
		operator_key int path 'x:OperatorKey',
		harvest_date timestamptz path 'x:HarvestDate',
		dbh int path 'x:SingleTreeProcessedStem/x:DBH',
		lat_cat_base text path 'x:StemCoordinates[@receiverPosition="Base machine position"][@coordinateReferenceSystem="WGS84"]/Latitude/@latitudeCategory',
		lat_base double precision path 'x:StemCoordinates[@receiverPosition="Base machine position"][@coordinateReferenceSystem="WGS84"]/Latitude',
		lon_cat_base text path 'x:StemCoordinates[@receiverPosition="Base machine position"][@coordinateReferenceSystem="WGS84"]/Longitude/@longitudeCategory',
		lon_base double precision path 'x:StemCoordinates[@receiverPosition="Base machine position"][@coordinateReferenceSystem="WGS84"]/Longitude',
		lat_cat_crane text path 'x:StemCoordinates[@receiverPosition="Crane tip position when felling the tree"][@coordinateReferenceSystem="WGS84"]/Latitude/@latitudeCategory',
		lat_crane double precision path 'x:StemCoordinates[@receiverPosition="Crane tip position when felling the tree"][@coordinateReferenceSystem="WGS84"]/Latitude',
		lon_cat_crane text path 'x:StemCoordinates[@receiverPosition="Crane tip position when felling the tree"][@coordinateReferenceSystem="WGS84"]/Longitude/@longitudeCategory',
		lon_crane double precision path 'x:StemCoordinates[@receiverPosition="Crane tip position when felling the tree"][@coordinateReferenceSystem="WGS84"]/Longitude'
	) r
ON CONFLICT DO NOTHING;


/*Insert Logs*/

row_path = '/x:HarvestedProduction/x:Machine['||machine_index||']/x:Stem/x:SingleTreeProcessedStem/x:Log';

INSERT INTO rawdata_xml.hpr_logs(filid,doc_mod_date, machine_key, stem_key, log_key, product_key,m3_price, m3_sob, m3_sub, length, butt_ob, butt_ub, mid_ob, mid_ub, top_ob, top_ub)
SELECT filid_val,doc_mod_date, mk, stem_key, log_key, product_key,m3_price, m3_sob, m3_sub, length, butt_ob, butt_ub, mid_ob, mid_ub, top_ob, top_ub
	FROM
	XMLTABLE(xmlnamespaces('urn:skogforsk:stanford2010' as x),
	row_path
	PASSING  d
	COLUMNS 
		stem_key int path '../../x:StemKey',
		log_key int path 'x:LogKey',
		product_key int path 'x:ProductKey',
		m3_price double precision path 'x:LogVolume[@logVolumeCategory="m3 (price)"][@logMeasurementCategory="Machine"]',
		m3_sob double precision path 'x:LogVolume[@logVolumeCategory="m3sob"][@logMeasurementCategory="Machine"]',
		m3_sub double precision path 'x:LogVolume[@logVolumeCategory="m3sub"][@logMeasurementCategory="Machine"]',
		length int path 'x:LogMeasurement/x:LogLength',
		butt_ob int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Butt ob"]',
		butt_ub int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Butt ub"]',
		mid_ob int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Mid ob"]',
		mid_ub int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Mid ub"]',
		top_ob int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Top ob"]',
		top_ub int path 'x:LogMeasurement/x:LogDiameter[@logDiameterCategory="Top ub"]'
) r
ON CONFLICT DO NOTHING;


END LOOP;
return 0;


END
$body$
LANGUAGE plpgsql;

